import sys
from PyQt5 import QtWidgets
import pandas as pd
from descargue import Ui_descargueTipificaciones
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime,timedelta
from time import sleep, time
import pandas as pd
from os import  remove
import win32com.client as win32
from alerta import Ui_Alerta



class MyApp(QtWidgets.QMainWindow):

    def __init__(self):
        super(MyApp, self).__init__()
        self.ui = Ui_descargueTipificaciones()
        self.ui.setupUi(self)
        self.start = True
        self.ui.pushButton.clicked.connect(self.buscarCaso)
        
        
        # self.uiA.mensaje.setText('Descargando tipificaciones mensuales')

    def buscarCaso(self):
        self.ventana = QtWidgets.QMainWindow()
        self.uiA = Ui_Alerta()
        self.uiA.setupUi(self.ventana)
        self.ventana.show()
        self.uiA.mensaje.setText('Descargando registros')
        sleep(1)

        now = datetime.now()
        now2 = datetime.now() - timedelta(days=30)
        fechaHoy = now.strftime("%d/%m/%Y")
        fechaMesAtras = now2.strftime("%d/%m/%Y")
        try:
            usuario = str(self.ui.usuario.text())
            contraseña = str(self.ui.contrasena.text())
            # usuario = '1023949557'
            # contraseña = 'Mayo2021*'
            
            if(usuario == '' or contraseña == ''):
                self.uiA.mensaje.setText('Credenciales incorrectas')
            else :
                opciones = webdriver.ChromeOptions()
                opciones.add_experimental_option(
                    'excludeSwitches', ['enable-automation'])
                opciones.add_experimental_option(
                    'prefs', {'download.default_directory': 'C:\\TipificacionesDescargadasMensual'})
                driver = webdriver.Chrome(
                    executable_path=r"driver/chromedriver.exe", chrome_options=opciones)
                driver.get(
                    "http://crm.colcomercio.com.co/vozcliente/index.php")
                driver.maximize_window()

                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.ID, 'usuario'))).send_keys(usuario)
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '/html/body/div/div/form/div[2]/input'))).send_keys(contraseña)
                WebDriverWait(driver, 5).until(
                    EC.element_to_be_clickable((By.ID, 'ingresa'))).click()

                for i in range(5):
                    try:
                        WebDriverWait(driver, 15).until(
                        EC.element_to_be_clickable((By.ID, 'menu-toggle'))).click()
                        WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '/html/body/div[1]/nav/div/div[3]/ol/li[4]/a'))).click()
                        break
                    except:
                        print('Retry in 3 second')
                        sleep(1)

                for i in range(5):
                    try:
                        driver.find_element_by_xpath(
                            '/html/body/div[1]/div/div/div[2]/div/form/div[1]/div/div[6]/input').click()
                        break
                    except:
                        print('Retry in 2 second')
                        sleep(1)

                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/div[1]/div[3]/span/div/button'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[13]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[11]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[9]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/div[1]/div[3]/span/div/ul/li[8]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[4]/a/label'))).click()

                # #Despachos
                # #******************
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="correo"]'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/button'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[9]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[10]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[11]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[12]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[13]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[23]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[27]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[28]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[29]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[30]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="fecha1"]'))).clear()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="fecha1"]'))).send_keys(fechaMesAtras)
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/button'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="generar"]'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="lnk_exporta"]'))).click()

                sleep(80)


                self.uiA.mensaje.setText('Descargando registro por favor espere')
                    

                # pandas
                # ***************
                self.uiA.mensaje.setText('Generando archivo .xlsx, por favor espere')
                
                fname = "C:\TipificacionesDescargadasMensual\REPORTE EXCEL PQR.xls"
                excel = win32.gencache.EnsureDispatch('Excel.Application')
                wb = excel.Workbooks.Open(fname)
                # FileFormat = 51 is for .xlsx extension
                wb.SaveAs(fname+"x", FileFormat=51)
                wb.Close()  # FileFormat = 56 is for .xls extension
                excel.Application.Quit()
                self.uiA.mensaje.setText('Buscando casos duplicados, por favor espere')

                now = datetime.now()
                fecha = now.strftime("%m-%d-%Y-%H-%M-%S")
                columnas = [0,5,21,40]
                df = pd.read_excel(
                    'C:\TipificacionesDescargadasMensual\REPORTE EXCEL PQR.xlsx', header=0, usecols=columnas)
                nuevodf = df[df['CREADO_POR'].str.contains(
                    "(Emtelco)", case=False)]
                # print(nuevodf)
                
                duplicados = df[df['NO_DESPACHO'].duplicated(keep=False)]['NO_DESPACHO'].tolist()
                duplicados = [x for x in duplicados if pd.isnull(x) == False]
                nuevodf['Duplicados'] = pd.Series(duplicados)
                nuevodf.to_excel('Reportes Mensuales Despachos/nuevoReporteDespachosMensualPQR_' + fecha + '.xlsx', index=False)

                remove('C:\TipificacionesDescargadasMensual\REPORTE EXCEL PQR.xls')
                remove('C:\TipificacionesDescargadasMensual\REPORTE EXCEL PQR.xlsx')

                self.uiA.mensaje.setText('Tipificacion modificada y guardada, Despachos')

                driver.back()

                # PostVenta
                # ***************
                for i in range(5):
                    try:
                        driver.find_element_by_xpath(
                            '/html/body/div[1]/div/div/div[2]/div/form/div[1]/div/div[6]/input').click()
                        break
                    except:
                        print('Retry in 2 second')
                        sleep(1)

                WebDriverWait(driver, 15).until(EC.element_to_be_clickable(
                    (By.XPATH, '/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/div[1]/div[3]/span/div/button'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[13]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[11]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[9]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/div[1]/div[3]/span/div/ul/li[8]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[4]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="correo"]'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/button'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[14]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[17]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[18]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[21]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[25]/a/label'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="fecha1"]'))).clear()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="fecha1"]'))).send_keys(fechaMesAtras)
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="generar"]'))).click()
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="lnk_exporta"]'))).click()

                sleep(80)
                driver.quit()

                ## Pandas
                # *****************************************************************************
                self.uiA.mensaje.setText('Generando archivo .xlsx, por favor espere')

                fname = "C:\TipificacionesDescargadasMensual\REPORTE EXCEL PQR.xls"
                excel = win32.gencache.EnsureDispatch('Excel.Application')
                wb = excel.Workbooks.Open(fname)
                # FileFormat = 51 is for .xlsx extension
                wb.SaveAs(fname+"x", FileFormat=51)
                wb.Close()  # FileFormat = 56 is for .xls extension
                excel.Application.Quit()

                now = datetime.now()
                fecha = now.strftime("%m-%d-%Y-%H-%M-%S")
                columnas = [0,5,6,18,19,40]
                df = pd.read_excel(
                    'C:\TipificacionesDescargadasMensual\REPORTE EXCEL PQR.xlsx', header=0, usecols=columnas)
                nuevodf = df[df['CREADO_POR'].str.contains(
                    "(Emtelco)", case=False)]
                
                duplicadosFactura = df[df['NO_FACTURA'].duplicated(keep=False)]['NO_FACTURA'].tolist()
                duplicadosFactura = [x for x in duplicadosFactura if pd.isnull(x) == False]
                nuevodf['Duplicados Factura'] = pd.Series(duplicadosFactura)
                nuevodf.to_excel('Reportes Mensuales PostVenta/nuevoReportePostVentaMensualPQR_' + fecha + '.xlsx', index=False)

                remove('C:\TipificacionesDescargadasMensual\REPORTE EXCEL PQR.xls')
                remove('C:\TipificacionesDescargadasMensual\REPORTE EXCEL PQR.xlsx')

                self.uiA.mensaje.setText('Tipificacion modificada y guardada, PostVenta')

        except:
            self.uiA.mensaje.setText('Error...')
            driver.quit()
            pass


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
