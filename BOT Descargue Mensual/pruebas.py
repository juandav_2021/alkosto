import sys
from PyQt5 import QtWidgets
import pandas as pd
from descargue import Ui_descargueTipificaciones
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime,timedelta
from time import sleep, time
import pandas as pd
from os import  remove
import win32com.client as win32


fname = "C:\TipificacionesDescargadasMensual\REPORTE.xls"
excel = win32.gencache.EnsureDispatch('Excel.Application')
wb = excel.Workbooks.Open(fname)
# FileFormat = 51 is for .xlsx extension
wb.SaveAs(fname+"x", FileFormat=51)
wb.Close()  # FileFormat = 56 is for .xls extension
excel.Application.Quit()

now = datetime.now()
fecha = now.strftime("%m-%d-%Y-%H-%M-%S")
columnas = [0,5,6,18,19,40]
df = pd.read_excel(
    'C:\TipificacionesDescargadasMensual\REPORTE.xlsx', header=0, usecols=columnas)
nuevodf = df[df['CREADO_POR'].str.contains(
    "(Emtelco)", case=False)]
# print(nuevodf)
print(nuevodf['NO_CASO'].duplicated().sum())
print(nuevodf['CLASE'].duplicated().sum())
print(nuevodf['SUB_CLASE'].duplicated().sum())
print(nuevodf['CREADO_POR'].duplicated().sum())
duplicadosFactura = df[df['NO_FACTURA'].duplicated(keep=False)]['NO_FACTURA'].tolist()
duplicadosFactura = [x for x in duplicadosFactura if pd.isnull(x) == False]
nuevodf['Duplicados Factura'] = pd.Series(duplicadosFactura)
nuevodf.to_excel('Reportes Mensuales PostVenta/nuevoReportePostVentaMensualPQR_' + fecha + '.xlsx', index=False)
print(nuevodf)


