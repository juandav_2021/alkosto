# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'App.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(821, 371)
        MainWindow.setMinimumSize(QtCore.QSize(821, 371))
        MainWindow.setMaximumSize(QtCore.QSize(821, 371))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/images/icak12.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet("*{\n"
"color: #222222;\n"
"font-family: century gothic;\n"
"}\n"
"\n"
"QWidget{\n"
"    background-color: #0077b6;\n"
"}\n"
"QFrame{\n"
"    background-color:#fffbf4 ;\n"
"    border-radius: 10px;\n"
"}\n"
"QLineEdit{\n"
"color:white;\n"
"border-radius:5px;\n"
"}\n"
"QPushButton{\n"
"background:#f07167;\n"
"color: white;\n"
"border-radius: 5px;\n"
"}\n"
"QPushButton:hover{\n"
"background:#ff9b85;\n"
"color: white;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton{\n"
"background-color:#fffbf4 ;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 10, 541, 351))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.entrada = QtWidgets.QLineEdit(self.frame)
        self.entrada.setGeometry(QtCore.QRect(120, 190, 271, 81))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(40)
        self.entrada.setFont(font)
        self.entrada.setAlignment(QtCore.Qt.AlignCenter)
        self.entrada.setObjectName("entrada")
        self.label_5 = QtWidgets.QLabel(self.frame)
        self.label_5.setGeometry(QtCore.QRect(60, 120, 411, 31))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(16)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.frame)
        self.label_6.setGeometry(QtCore.QRect(5, 150, 531, 31))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.pushButton = QtWidgets.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(200, 290, 121, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(16)
        self.pushButton.setFont(font)
        self.pushButton.setObjectName("pushButton")
        self.label_4 = QtWidgets.QLabel(self.frame)
        self.label_4.setGeometry(QtCore.QRect(30, 50, 461, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(30)
        font.setItalic(True)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setGeometry(QtCore.QRect(560, 10, 251, 171))
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.label = QtWidgets.QLabel(self.frame_2)
        self.label.setGeometry(QtCore.QRect(90, 20, 71, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(20)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.frame_2)
        self.label_2.setGeometry(QtCore.QRect(60, 70, 131, 71))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(46)
        self.label_2.setFont(font)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.frame_2)
        self.label_3.setGeometry(QtCore.QRect(15, 170, 221, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(20)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.resultado = QtWidgets.QLabel(self.frame_2)
        self.resultado.setGeometry(QtCore.QRect(45, 210, 161, 71))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(46)
        self.resultado.setFont(font)
        self.resultado.setAlignment(QtCore.Qt.AlignCenter)
        self.resultado.setObjectName("resultado")
        self.toolButton = QtWidgets.QToolButton(self.frame_2)
        self.toolButton.setGeometry(QtCore.QRect(200, 300, 41, 41))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/images/images/icono5.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton.setIcon(icon1)
        self.toolButton.setIconSize(QtCore.QSize(36, 36))
        self.toolButton.setObjectName("toolButton")
        self.frame_3 = QtWidgets.QFrame(self.centralwidget)
        self.frame_3.setGeometry(QtCore.QRect(560, 190, 251, 171))
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.label_9 = QtWidgets.QLabel(self.frame_3)
        self.label_9.setGeometry(QtCore.QRect(15, 20, 221, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(20)
        self.label_9.setFont(font)
        self.label_9.setObjectName("label_9")
        self.resultado_2 = QtWidgets.QLabel(self.frame_3)
        self.resultado_2.setGeometry(QtCore.QRect(45, 70, 161, 71))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(46)
        self.resultado_2.setFont(font)
        self.resultado_2.setAlignment(QtCore.Qt.AlignCenter)
        self.resultado_2.setObjectName("resultado_2")
        self.toolButton_2 = QtWidgets.QToolButton(self.frame_3)
        self.toolButton_2.setGeometry(QtCore.QRect(200, 300, 41, 41))
        self.toolButton_2.setIcon(icon1)
        self.toolButton_2.setIconSize(QtCore.QSize(36, 36))
        self.toolButton_2.setObjectName("toolButton_2")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Promedio ATH"))
        self.label_5.setText(_translate("MainWindow", "Ingresa el tiempo de tu ultima llamada:"))
        self.label_6.setText(_translate("MainWindow", "Separa los minutos de los segundos con un punto, ejemplo 4.3 equivale a 4 minutos y 30 segundos"))
        self.pushButton.setText(_translate("MainWindow", "Agregar"))
        self.label_4.setText(_translate("MainWindow", "Promedio ATH Llamada"))
        self.label.setText(_translate("MainWindow", "Meta"))
        self.label_2.setText(_translate("MainWindow", "320"))
        self.label_3.setText(_translate("MainWindow", "Tu estado actual"))
        self.resultado.setText(_translate("MainWindow", "00:00"))
        self.toolButton.setText(_translate("MainWindow", "..."))
        self.label_9.setText(_translate("MainWindow", "Tu estado actual"))
        self.resultado_2.setText(_translate("MainWindow", "00:00"))
        self.toolButton_2.setText(_translate("MainWindow", "..."))
import source


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
