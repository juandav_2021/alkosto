from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QMovie
import sys

class alerta(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(250, 250)
        MainWindow.setMinimumSize(QtCore.QSize(250, 250))
        MainWindow.setMaximumSize(QtCore.QSize(250, 250))
        MainWindow.setStyleSheet("*{\n"
"color: #222222;\n"
"font-family: century gothic;\n"
"}\n"
"\n"
"QWidget{\n"
"    background-color: #0077b6;\n"
"}\n"
"QFrame{\n"
"    background-color:#fffbf4 ;\n"
"    border-radius: 10px;\n"
"}\n"
"QLineEdit{\n"
"color:white;\n"
"border-radius:5px;\n"
"}\n"
"QPushButton{\n"
"background:#f07167;\n"
"color: white;\n"
"border-radius: 5px;\n"
"}\n"
"QPushButton:hover{\n"
"background:#ff9b85;\n"
"color: white;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton{\n"
"background-color:#fffbf4 ;\n"
"border-radius: 5px;\n"
"}\n"
"")
        self.frame = QtWidgets.QFrame(MainWindow)
        self.frame.setGeometry(QtCore.QRect(10, 10, 281, 281))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.label2 = QtWidgets.QLabel(self.frame)
        self.label2.setGeometry(QtCore.QRect(15, 10, 251, 21))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(13)
        self.label2.setFont(font)
        self.label2.setObjectName("label")
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        # create label
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(25, 25, 200, 200))
        self.label.setMinimumSize(QtCore.QSize(200, 200))
        self.label.setMaximumSize(QtCore.QSize(200, 200))
        self.label.setObjectName("label")

        # add label to main window
        MainWindow.setCentralWidget(self.centralwidget)

        # set qmovie as label
        self.movie = QMovie("gifs/prueba2.gif")
        self.label.setMovie(self.movie)
        self.movie.start()
        
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("Alerta", "Alerta"))

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = QtWidgets.QMainWindow()
    ui = alerta()
    ui.setupUi(window)
    window.show()
    sys.exit(app.exec_())