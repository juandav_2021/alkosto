import sys
from PyQt5 import uic, QtWidgets
from Resultados import Ui_Resultados
from Seleccionar import Ui_selecionarCaso
from Casos import Ui_MainWindow
import pandas as pd
# import openpyxl
from datetime import date, datetime


class MyApp(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyApp, self).__init__()
        self.uiR = Ui_Resultados()
        self.uiR.setupUi(self)
        self.start = True
        self.uiR.buscar.clicked.connect(self.buscarCaso)
        self.uiC = Ui_selecionarCaso()

    def buscarCaso(self):
        self.ventana = QtWidgets.QMainWindow()
        self.uiC = Ui_selecionarCaso()
        self.uiC.setupUi(self.ventana)
        self.ventana.show()
        self.uiC.pushButton.clicked.connect(self.buscarResultado)

        hoy = date.today()
        año = str(hoy.year)
        # ==================================================>
        nombreUsuario =  self.uiR.nombre.text()
        print(nombreUsuario)
        dia = self.uiR.dia.text()
        # ==================================================>
        mes = self.uiR.meses.selectedItems()
        # ==================================================>
        diaDesde = self.uiR.dia_2.text()
        diaHasta = self.uiR.dia_3.text()
        # ==================================================>
        mesSelecionado = []
        # print(mesSelecionado.append(mes[0].text()))
        try:
            for i in range(len(mes)):
                mesSelecionado.append(mes[i].text())
                print(mesSelecionado[0])
                if (mesSelecionado[0] == 'Enero'):
                    mesSelecionado[0] = '01-'
                elif (mesSelecionado[0] == 'Febrero'):
                    mesSelecionado[0] = '02-'
                elif (mesSelecionado[0] == 'Marzo'):
                    mesSelecionado[0] = '03-'
                elif (mesSelecionado[0] == 'Abril'):
                    mesSelecionado[0] = '04-'
                elif (mesSelecionado[0] == 'Mayo'):
                    mesSelecionado[0] = '05-'
                elif (mesSelecionado[0] == 'Junio'):
                    mesSelecionado[0] = '06-'
                elif (mesSelecionado[0] == 'Julio'):
                    mesSelecionado[0] = '07-'
                elif (mesSelecionado[0] == 'Agosto'):
                    mesSelecionado[0] = '08-'
                elif (mesSelecionado[0] == 'Septiembre'):
                    mesSelecionado[0] = '09-'
                elif (mesSelecionado[0] == 'Octubre'):
                    mesSelecionado[0] = '10-'
                elif (mesSelecionado[0] == 'Noviembre'):
                    mesSelecionado[0] = '11-'
                elif (mesSelecionado[0] == 'Diciembre'):
                    mesSelecionado[0] = '12-'
            # ==================================================>
            documento = pd.read_excel(
                "doc/PLANTILLA PARA AUTOMATIZACIÓN RESULTADOS DIARIOS.xlsx", sheet_name="Hoja2", header=0)
            documento =  documento[documento['CREADOR'] == nombreUsuario]
            # ==================================================>
            if(dia != ''):
                if(len(dia) <= 1):
                    dia = '0'+dia
                    fecha = año+'-'+mesSelecionado[0]+dia
                else:
                    fecha = año+'-'+mesSelecionado[0]+dia
                print(fecha)
                documentoFechas = documento[documento['FECHA'] == fecha]
                print(documentoFechas)
            elif(diaDesde != '' and diaHasta != ''):
                if(len(diaDesde) <= 1):
                    diaDesde = '0'+diaDesde
                if(len(diaHasta) <= 1):
                    diaHasta = '0'+diaHasta
                fechaDesde = año+'-'+mesSelecionado[0]+diaDesde
                fechaHasta = año+'-'+mesSelecionado[0]+diaHasta
                print(fechaDesde, fechaHasta)
                documentoFechas = documento.loc[documento['FECHA'].between(
                    fechaDesde, fechaHasta)]
                print(documentoFechas)
            elif(dia == '' and diaDesde == '' and diaHasta == ''):
                if(mesSelecionado[0] == '02-'):
                    fechaDesde = año+'-'+mesSelecionado[0]+'01'
                    fechaHasta = año+'-'+mesSelecionado[0]+'28'
                    print(dia, diaDesde, diaHasta)
                    print(fechaDesde, fechaHasta)
                    documentoFechas = documento.loc[documento['FECHA'].between(fechaDesde, fechaHasta)]
                    print(documentoFechas)
                elif(mesSelecionado[0] == '04-' or mesSelecionado[0] == '06-' or mesSelecionado[0] == '09-' or mesSelecionado[0] == '11-'):
                    fechaDesde = año+'-'+mesSelecionado[0]+'01'
                    fechaHasta = año+'-'+mesSelecionado[0]+'30'
                    print(dia, diaDesde, diaHasta)
                    print(fechaDesde, fechaHasta)
                    documentoFechas = documento.loc[documento['FECHA'].between(fechaDesde, fechaHasta)]
                    print(documentoFechas)
                else:
                    fechaDesde = año+'-'+mesSelecionado[0]+'01'
                    fechaHasta = año+'-'+mesSelecionado[0]+'31'
                    print(dia, diaDesde, diaHasta)
                    print(fechaDesde, fechaHasta)
                    documentoFechas = documento.loc[documento['FECHA'].between(fechaDesde, fechaHasta)]
                    print(documentoFechas)
            # ==================================================>

            dia = self.uiR.dia.setText('')
            diaDesde = self.uiR.dia_2.setText('')
            diaHasta = self.uiR.dia_3.setText('')

            casos = documentoFechas["NO CASO"].tolist()
            for i in range(len(casos)):
                self.uiC.listWidget.addItem(str(casos[i]))

            casoSelecionado = self.uiC.listWidget.selectedItems()
        except:
            pass

    def buscarResultado(self):
        self.ventana2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.ventana2)
        self.ventana2.show()
        items = self.uiC.listWidget.selectedItems()
        selecion = []
        try:
            for x in range(len(items)):
                selecion.append(self.uiC.listWidget.selectedItems()[x].text())
                self.ui.label_8.setText(selecion[0])

            documento = pd.read_excel(
                "doc/PLANTILLA PARA AUTOMATIZACIÓN RESULTADOS DIARIOS.xlsx", sheet_name="Hoja2", header=0)
            caso = selecion[0]
            # print(caso)

            documento = documento[documento['NO CASO'] == (caso)]
            # print(documento)
            creador = documento["CREADOR"].tolist()
            area = documento["AREA"].tolist()
            error = documento["ERROR"].tolist()
            retroalimentacion = documento["RETROALIMENTACION"].tolist()
            reviso = documento["REVISO"].tolist()
            observaciones = documento["OBSERVACIONES"].tolist()
            fecha = documento["FECHA"].tolist()
            fecha = fecha[0].to_pydatetime().date()
            # dt_object = datetime.fromtimestamp(fecha[0])
            # day = datetimeobj.strftime("%d")
            print(fecha)
            self.ui.textBrowser_5.setText(creador[0])
            self.ui.label_10.setText(area[0])
            self.ui.textBrowser_2.setText(error[0])
            self.ui.textBrowser_3.setText(retroalimentacion[0])
            self.ui.textBrowser_4.setText(reviso[0])
            self.ui.textBrowser.setText(observaciones[0])
            self.ui.textBrowser_6.setText(str(fecha))
        except:
            print('Selecion no valida')


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
