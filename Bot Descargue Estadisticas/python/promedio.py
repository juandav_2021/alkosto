from numpy import unique
import pandas as pd
df = pd.read_excel('C:\\Datos Bot Preguntas\\01_Encuesta_Satisfaccion_Online.xlsx',
                   header=12, usecols=[1, 7, 8, 11, 13, 14])
df = pd.DataFrame(df)
listaAsesores = df['Asesor'].unique().tolist()
nuevodf = df[df['Asesor'] == 'ccanters']
print(nuevodf)
nuevodf = df
print(nuevodf)


# Pregunta 1
print('total')
total = len(nuevodf['Pregunta1'])
print(total)
print('Retractores')
retractores = len(nuevodf['Pregunta1'].loc[nuevodf['Pregunta1'] <= 6])
print(retractores)
print('neutros')
neutros = nuevodf[(nuevodf['Pregunta1'].loc[nuevodf['Pregunta1'] >= 7]) & (
    nuevodf['Pregunta1'].loc[nuevodf['Pregunta1'] <= 8])]
neutros = len(neutros['Pregunta1'])
print(neutros)
print('Prometedores')
prometerores = len(nuevodf['Pregunta1'].loc[nuevodf['Pregunta1'] >= 9])
print(prometerores)
promedio = (prometerores - retractores)/total
print('El promedio es de:')
print(promedio*100)

# Pregunta 2
print('total')
total = nuevodf['Pregunta2'].mean()
print(round(total, 2))

# Pregunta 3
print('total')
total = nuevodf['Pregunta3'].mean()
print(total)

# Pregunta 4
print('total')
total = nuevodf['Pregunta4'].mean()
print(total)


# def graficarPregunta(self):
#     print('ok')
#     df = pd.read_excel('C:\\Datos Bot Preguntas\\01_Encuesta_Satisfaccion_Online.xlsx',
#                        header=12, usecols=[1, 7, 8, 11, 13])
#     df = pd.DataFrame(df)

#     # -----------------------------------------------------------------------------------------------------------
#     seleccion = self.ui.lista.selectedItems()
#     seleccionNueva = []

#     print(seleccion)
#     for i in range(len(seleccion)):
#         seleccionNueva.append(seleccion[i].text())
#         print(seleccionNueva[i])
#         if seleccionNueva[i] == 'Total Preguntas':

#             pregunta1 = df['Pregunta1'].mean()
#             pregunta2 = df['Pregunta2'].mean()
#             pregunta3 = df['Pregunta3'].mean()
#             pregunta4 = df['Pregunta4'].mean()

#             totalPreguntas = [pregunta1, pregunta2, pregunta3, pregunta4]
#             nombres = ['Pregunta1', 'pregunta2', 'pregunta3', 'pregunta4']
#             dfnuevo = pd.DataFrame({"Preguntas": nombres,
#                                     "Calificaciones": totalPreguntas})
#             print(dfnuevo)
#             plt.barh(dfnuevo['Preguntas'], dfnuevo['Calificaciones'], color=[
#                      'red', 'orange', 'green', 'blue'])
#             plt.show()
#         elif seleccionNueva[i] == 'Pregunta 1':
#             # solo una pregunta
#             df.plot.bar(x='Fecha', y='Pregunta1')
#             # Legenda en el eje y
#             plt.ylabel('Calificación Pregunta')

#             # Legenda en el eje x
#             plt.xlabel('Fecha de las llamadas')

#             # Título de Gráfica
#             plt.title('Pregunta 1')
#             plt.show()
#         elif seleccionNueva[i] == 'Pregunta 2':
#             # solo una pregunta
#             df.plot.bar(x='Fecha', y='Pregunta2')
#             # Legenda en el eje y
#             plt.ylabel('Calificación Pregunta')

#             # Legenda en el eje x
#             plt.xlabel('Fecha de las llamadas')

#             # Título de Gráfica
#             plt.title('Pregunta 1')
#             plt.show()
#         elif seleccionNueva[i] == 'Pregunta 3':
#             # solo una pregunta
#             df.plot.bar(x='Fecha', y='Pregunta3')
#             # Legenda en el eje y
#             plt.ylabel('Calificación Pregunta')

#             # Legenda en el eje x
#             plt.xlabel('Fecha de las llamadas')

#             # Título de Gráfica
#             plt.title('Pregunta 1')
#             plt.show()
#         elif seleccionNueva[i] == 'Pregunta 4':
#             # solo una pregunta
#             df.plot.bar(x='Fecha', y='Pregunta4')
#             # Legenda en el eje y
#             plt.ylabel('Calificación Pregunta')

#             # Legenda en el eje x
#             plt.xlabel('Fecha de las llamadas')

#             # Título de Gráfica
#             plt.title('Pregunta 1')
#             plt.show()
