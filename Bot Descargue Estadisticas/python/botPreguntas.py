import sys
from PyQt5 import QtWidgets
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
import pandas as pd
import matplotlib.pyplot as plt
from calificacionPreguntas import Ui_Form
from resultadoGeneralPreguntas import Ui_resultados
from resultadoPreguntasCreador import Ui_MainWindow
from os import  remove



class Descargue(QtWidgets.QMainWindow):
    def __init__(self):
        super(Descargue, self).__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.strat = True
        self.ui.actualizar.clicked.connect(self.testDescargueRegistros)
        self.ui.mostrarGeneral.clicked.connect(self.calificacionesGenerales)
        self.ui.mostrarCreador_2.clicked.connect(self.calificacionesPorCreador)
        self.ui.mostrarCreador.clicked.connect(self.seleccionCreador)
        self.ui.lideres.clicked.connect(self.seleccionarLider)
        
    def testDescargueRegistros(self):
        try:
            remove('C:\\Datos Bot Preguntas\\01_Encuesta_Satisfaccion_Online.xlsx')
        except:
            pass
        try:
            opciones = webdriver.ChromeOptions()
            opciones.add_experimental_option('excludeSwitches', ['enable-automation'])
            opciones.add_experimental_option(
                    'prefs', {'download.default_directory': 'C:\\Datos Bot Preguntas'})
            self.driver = webdriver.Chrome(
                            executable_path=r"controller/chromedriver.exe", chrome_options=opciones)
            
            driver =  self.driver
            driver.maximize_window()
            driver = driver.get("http://reportesemtelco/Reportes/Pages/Report.aspx?ItemPath=%2fALKOSTO%2fIVR%2fEntrada%2f01_Encuesta_Satisfaccion_Online")
        
            driver =  self.driver
            WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ctl32_ctl04_ctl03_ddDropDownButton"]'))).click()
            WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ctl32_ctl04_ctl03_divDropDown_ctl00"]'))).click()
            WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ctl32_ctl04_ctl00"]'))).click()
            sleep(3)

            for i in range(5):
                try:
                    WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.XPATH, '/html/body/form/span[1]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/div[2]/div/table/tbody/tr[4]/td/div/div/div[3]/table/tbody/tr/td/div[1]/table/tbody/tr/td/a/img[1]')))\
                    .click()
                    elemento = driver.find_element_by_css_selector('#ctl32_ctl05_ctl04_ctl00_Menu > div:nth-child(5) > a')
                    elemento.click()
                    # print('click')
                    break
                except:
                    # print('No se encuentra, si intentara en 1 segundo')
                    sleep(1)

            sleep(3)
            driver.quit()
            
        except:
            driver.quit()
            pass

    def calificacionesGenerales(self):
        self.ventana = QtWidgets.QMainWindow()
        self.uiC = Ui_resultados()
        self.uiC.setupUi(self.ventana)
        self.ventana.show()

        try:
            df = pd.read_excel('C:\\Datos Bot Preguntas\\01_Encuesta_Satisfaccion_Online.xlsx',
                   header=12, usecols=[1, 7, 8, 11, 13, 14])
            seleccion = self.ui.lista.selectedItems()
            for i in range(len(seleccion)):
                seleccion = seleccion[i].text()
                # print(seleccion)
            if seleccion == 'Pregunta 1':
                # print('total')
                total = len(df['Pregunta1'])
                # print(total)
                # print('Retractores')
                retractores = len(df['Pregunta1'].loc[df['Pregunta1'] <= 6])
                # print(retractores)
                # print('neutros')
                neutros = df[(df['Pregunta1'].loc[df['Pregunta1'] >= 7]) & (
                    df['Pregunta1'].loc[df['Pregunta1'] <= 8])]
                neutros = len(neutros['Pregunta1'])
                # print(neutros)
                # print('Prometedores')
                prometerores = len(df['Pregunta1'].loc[df['Pregunta1'] >= 9])
                # print(prometerores)
                promedio = (prometerores - retractores)/total
                # print('El promedio es de:')
                promedio = promedio*100
                promedio = round(promedio,2)
                # print(promedio)
                self.uiC.promedio.setText(str(promedio) + ' %')

            elif seleccion == 'Pregunta 2':
                # print('total')
                total = df['Pregunta2'].mean()
                total = round(total, 2)
                print(total)
                self.uiC.promedio.setText(str(total))

            elif seleccion == 'Pregunta 3':
                # print('total')
                total = df['Pregunta3'].mean()
                total = round(total, 2)
                # print(total)
                self.uiC.promedio.setText(str(total))

            elif seleccion == 'Pregunta 4':
                # print('total')
                total = df['Pregunta4'].mean()
                total = round(total, 2)
                # print(total)
                self.uiC.promedio.setText(str(total))
        except:
            pass
    
    def seleccionarLider(self):
        df = pd.read_excel('C:\\Datos Bot Preguntas\\Planta activa Alkosto.xlsx', sheet_name='Planta Activa', usecols=[3,5], header=0)
        lideres = df['Supervisor'].unique().tolist()
        # print(lideres)
        
        for i in range(len(lideres)):
            self.ui.listWidget.addItem(str(lideres[i]))
            # print(lideres[i]) 

    def calificacionesPorCreador(self):
        try:
            df = pd.read_excel('C:\\Datos Bot Preguntas\\Planta activa Alkosto.xlsx', sheet_name='Planta Activa', usecols=[3,5], header=0)
            creadorLider = self.ui.listWidget.selectedItems()
            for i in range(len(creadorLider)):
                creadorLider = creadorLider[i].text()

            creadoresPorLider = df[df['Supervisor'] == creadorLider]
            creadoresPorLider = creadoresPorLider['UsuariodeRed'].tolist() 
            # print(creadoresPorLider)
            
            for i in range(len(creadoresPorLider)):
                self.ui.lista_2.addItem(str(creadoresPorLider[i]))
                # print(creadoresPorLider[i])
        except:
            print('Error')
            pass

    def seleccionCreador(self):
        df = pd.read_excel('C:\\Datos Bot Preguntas\\01_Encuesta_Satisfaccion_Online.xlsx',
                header=12, usecols=[1, 7, 8, 11, 13, 14])
        df = pd.DataFrame(df)
        creadorSelecionado = self.ui.lista_2.selectedItems()
        for i in range(len(creadorSelecionado)):
            creadorSelecionado = creadorSelecionado[i].text()
        
        try:
            df = df[df['Asesor'] == creadorSelecionado]
            # print(df)
            self.ventana = QtWidgets.QMainWindow()
            self.uiC = Ui_MainWindow()
            self.uiC.setupUi(self.ventana)
            self.ventana.show()
            # Pregunta 1
            # print('total')
            total = len(df['Pregunta1'])
            # print(total)
            # print('Retractores')
            retractores = len(df['Pregunta1'].loc[df['Pregunta1'] <= 6])
            # print(retractores)
            # print('neutros')
            neutros = df[(df['Pregunta1'].loc[df['Pregunta1'] >= 7]) & (
                df['Pregunta1'].loc[df['Pregunta1'] <= 8])]
            neutros = len(neutros['Pregunta1'])
            # print(neutros)
            # print('Prometedores')
            prometerores = len(df['Pregunta1'].loc[df['Pregunta1'] >= 9])
            # print(prometerores)
            promedio = (prometerores - retractores)/total
            # print('El promedio es de:')
            # print(promedio)
            promedio = promedio * 100
            promedio = round(promedio,2)
            # print(promedio)
            self.uiC.resultadoPregunta1.setText(str(promedio)+' %')

            #Pregunta 2
            # print('total')
            total = df['Pregunta2'].mean()
            total = round(total, 2)
            # print(total)
            self.uiC.resultadoPregunta2.setText(str(total))

            #Pregunta 3
            # print('total')
            total = df['Pregunta3'].mean()
            total = round(total, 2)
            # print(total)
            self.uiC.resultadoPregunta3.setText(str(total))

            #Pregunta 4
            # print('total')
            total = df['Pregunta4'].mean()
            total = round(total, 2)
            # print(total)
            self.uiC.resultadoPregunta4.setText(str(total))
        except:
            # print('Error')
            pass


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Descargue()
    window.show()
    sys.exit(app.exec_())
        