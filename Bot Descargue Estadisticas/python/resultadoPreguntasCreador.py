# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'resultadoPreguntasCreador.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(529, 481)
        MainWindow.setMaximumSize(QtCore.QSize(529, 481))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setBold(True)
        font.setWeight(75)
        MainWindow.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/newPrefix/icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet("*{\n"
"color: #222222;\n"
"font-family: century gothic;\n"
"}\n"
"\n"
"QWidget{\n"
"    background-color: #f75e25;\n"
"}\n"
"QFrame{\n"
"    background-color:#fffbf4 ;\n"
"    border-radius: 10px;\n"
"}\n"
"\n"
"QLabel#titulo{\n"
"color: #3E94B2;\n"
"}\n"
"QLabel#resultadoPregunta1{\n"
"color: #3E94B2;\n"
"}\n"
"QLabel#resultadoPregunta2{\n"
"color: #3E94B2;\n"
"}\n"
"QLabel#resultadoPregunta3{\n"
"color: #3E94B2;\n"
"}\n"
"QLabel#resultadoPregunta4{\n"
"color: #3E94B2;\n"
"}\n"
"QLabel{\n"
"color: rgb(255, 85, 0);\n"
"}\n"
"\n"
"QPushButton{\n"
"background:#0473da;\n"
"color: white;\n"
"border-radius: 5px;\n"
"}\n"
"QPushButton:hover{\n"
"background:#024a8a;\n"
"color: white;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton{\n"
"background-color:#fffbf4 ;\n"
"border-radius: 5px;\n"
"}\n"
"Line{\n"
"background: rgb(0, 0, 0)\n"
"}\n"
"\n"
"")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 10, 511, 131))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.titulo = QtWidgets.QLabel(self.frame)
        self.titulo.setGeometry(QtCore.QRect(35, 0, 441, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(17)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.titulo.setFont(font)
        self.titulo.setAlignment(QtCore.Qt.AlignCenter)
        self.titulo.setObjectName("titulo")
        self.resultadoPregunta1 = QtWidgets.QLabel(self.frame)
        self.resultadoPregunta1.setGeometry(QtCore.QRect(195, 90, 121, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(24)
        font.setBold(True)
        font.setWeight(75)
        self.resultadoPregunta1.setFont(font)
        self.resultadoPregunta1.setAlignment(QtCore.Qt.AlignCenter)
        self.resultadoPregunta1.setObjectName("resultadoPregunta1")
        self.pregunta1 = QtWidgets.QLabel(self.frame)
        self.pregunta1.setGeometry(QtCore.QRect(160, 40, 191, 41))
        self.pregunta1.setMaximumSize(QtCore.QSize(191, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(26)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.pregunta1.setFont(font)
        self.pregunta1.setObjectName("pregunta1")
        self.toolButton = QtWidgets.QToolButton(self.frame)
        self.toolButton.setGeometry(QtCore.QRect(10, 10, 25, 19))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/newPrefix/16642.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton.setIcon(icon1)
        self.toolButton.setObjectName("toolButton")
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setGeometry(QtCore.QRect(10, 149, 511, 101))
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.pregunta2 = QtWidgets.QLabel(self.frame_2)
        self.pregunta2.setGeometry(QtCore.QRect(160, 10, 191, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(26)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.pregunta2.setFont(font)
        self.pregunta2.setObjectName("pregunta2")
        self.resultadoPregunta2 = QtWidgets.QLabel(self.frame_2)
        self.resultadoPregunta2.setGeometry(QtCore.QRect(195, 60, 121, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(24)
        font.setBold(True)
        font.setWeight(75)
        self.resultadoPregunta2.setFont(font)
        self.resultadoPregunta2.setAlignment(QtCore.Qt.AlignCenter)
        self.resultadoPregunta2.setObjectName("resultadoPregunta2")
        self.frame_3 = QtWidgets.QFrame(self.centralwidget)
        self.frame_3.setGeometry(QtCore.QRect(10, 259, 511, 101))
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.pregunta3 = QtWidgets.QLabel(self.frame_3)
        self.pregunta3.setGeometry(QtCore.QRect(160, 10, 191, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(26)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.pregunta3.setFont(font)
        self.pregunta3.setObjectName("pregunta3")
        self.resultadoPregunta3 = QtWidgets.QLabel(self.frame_3)
        self.resultadoPregunta3.setGeometry(QtCore.QRect(195, 60, 121, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(24)
        font.setBold(True)
        font.setWeight(75)
        self.resultadoPregunta3.setFont(font)
        self.resultadoPregunta3.setAlignment(QtCore.Qt.AlignCenter)
        self.resultadoPregunta3.setObjectName("resultadoPregunta3")
        self.frame_4 = QtWidgets.QFrame(self.centralwidget)
        self.frame_4.setGeometry(QtCore.QRect(10, 370, 511, 101))
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.pregunta4 = QtWidgets.QLabel(self.frame_4)
        self.pregunta4.setGeometry(QtCore.QRect(160, 10, 191, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(26)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.pregunta4.setFont(font)
        self.pregunta4.setObjectName("pregunta4")
        self.resultadoPregunta4 = QtWidgets.QLabel(self.frame_4)
        self.resultadoPregunta4.setGeometry(QtCore.QRect(195, 60, 121, 41))
        font = QtGui.QFont()
        font.setFamily("century gothic")
        font.setPointSize(24)
        font.setBold(True)
        font.setWeight(75)
        self.resultadoPregunta4.setFont(font)
        self.resultadoPregunta4.setAlignment(QtCore.Qt.AlignCenter)
        self.resultadoPregunta4.setObjectName("resultadoPregunta4")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Promedio Preguntas por Creador"))
        self.titulo.setText(_translate("MainWindow", "Promedio de las preguntas por creador"))
        self.resultadoPregunta1.setText(_translate("MainWindow", "00.00 %"))
        self.pregunta1.setText(_translate("MainWindow", "Pregunta 1"))
        self.toolButton.setText(_translate("MainWindow", "..."))
        self.pregunta2.setText(_translate("MainWindow", "Pregunta 2"))
        self.resultadoPregunta2.setText(_translate("MainWindow", "00.00 %"))
        self.pregunta3.setText(_translate("MainWindow", "Pregunta 3"))
        self.resultadoPregunta3.setText(_translate("MainWindow", "00.00 %"))
        self.pregunta4.setText(_translate("MainWindow", "Pregunta 4"))
        self.resultadoPregunta4.setText(_translate("MainWindow", "00.00 %"))
import source


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
