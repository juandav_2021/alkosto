from logging import error
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from time import sleep
import pandas as pd

df = pd.read_excel("doc/Base Bot.xlsx", sheet_name="Sheet1", header=1)
guias = df['Guía de envío'].tolist()
fechas = []
direcciones = []
eventos = []            
for i in range(len(guias)):
    try:
        numeroGuiaEnvio = guias[i]
        print(guias[i])
        opciones = webdriver.ChromeOptions()
        opciones.add_experimental_option('excludeSwitches', ['enable-automation'])
        driver = webdriver.Chrome(executable_path=r"driver/chromedriver.exe", chrome_options= opciones)
        driver.get("https://enviosonline.4-72.com.co/envios472/portal/rastrear.php?guia=UA320549853TRO&g-recaptcha-response=03AGdBq25nUkYJlUKw0rAPRfaL9JsjfFr4OYR4TUQMYFl7yQ9oCBFSpEs6N5gw7fV_BgeMVLpg6ZdwwX8BcRil5JZtP7NjyBNlkXBPy8shJyNGkR-brNzfC8038-4xfToaCByMZ489HBVZf0dAbcNzfJuAyF5SGuB8t6j3WNVNAP05FJIniE_JTj_DUoU4bjjO8SI6Iwuz0-Aw1SV9fuCuUmP46aXZtLmrgz-uE3x0to3i3VhvEjSdPuFS7ceL_gcbgLIonctfPR0VBl0SQemurQWPb5zXfbNl5kmhuVhnq0I6KWguUERMCUuns6ILz_45ETTBVKhlmVcFKyJOXNcI1sYi96GhRAo2VldzFrrFJ9ARsx5JEwqEwBTqFats1mCmNVqdVqSByWkqm_x4PwqdfMONPZJ2xHEGnaB3VdXuMflkeiG24sxYT7IoD3at58Gf_ZKyhMEePdg-cFMOI-9BIeDOKdwiX1bO1PQId-Rk9-BBPqBCS_rhPnc#") 
        driver.maximize_window()
        
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/article/form/div[1]/input')))\
                .send_keys(numeroGuiaEnvio,Keys.ENTER)
        sleep(1)
        WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="full-content"]/article/div[3]/a[2]')))\
                .click()
        WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="full-content"]/article/div[3]/a[2]')))\
                .click()
        WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="full-content"]/article/div[3]/a[2]')))\
                .click()
        
        # sleep(3)
        driver.switch_to.window(driver.window_handles[3])
        fechaEnvio = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH,'/html/body/form/table[2]/tbody/tr/td/span/div/table/tbody/tr[4]/td[3]/div/div[1]/div/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[5]/table/tbody/tr/td/div'))).text
        direccion = WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.XPATH,'/html/body/form/table[2]/tbody/tr/td/span/div/table/tbody/tr[4]/td[3]/div/div[1]/div/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[34]/td[4]/table/tbody/tr/td/div'))).text
        tabla = WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.XPATH,'/html/body/form/table[2]/tbody/tr/td/span/div/table/tbody/tr[4]/td[3]/div/div[1]/div/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[41]'))).text
        tabla = tabla.split('\n')
        driver.quit()
        fechas.append(fechaEnvio)
        direcciones.append(direccion)
        eventos.append(tabla[-2])
    except:
        fechas.append('')
        direcciones.append('')
        eventos.append('')
        print('error')
        driver.quit()

df['Fecha de Envio'] = fechas
df['Dirección'] = direcciones
df['Evento'] = eventos


print('Registros Guardados')
df = pd.DataFrame(df)
j = len(guias)
i = i+j

df.to_excel('guiasCompletas/nuevaBaseBot'+str(i)+'.xlsx', sheet_name='RegistrosCompletos', index=False)
