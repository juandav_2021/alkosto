import sys
from PyQt5 import QtWidgets
import pandas as pd
from descargue import Ui_descargueTipificaciones
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
from time import sleep
import pandas as pd
from os import remove
import win32com.client as win32


class MyApp(QtWidgets.QMainWindow):

    def __init__(self):
        super(MyApp, self).__init__()
        self.ui = Ui_descargueTipificaciones()
        self.ui.setupUi(self)
        self.start = True
        self.ui.pushButton.clicked.connect(self.buscarCaso)

    def buscarCaso(self):
        try:
            contador = 1
            while True:
                try:
                    usuario = str(self.ui.usuario.text())
                    contraseña = str(self.ui.contrasena.text())
                    # usuario = '1023949557'
                    # contraseña = 'Mayo2021*'
                
                    if(usuario == '' or contraseña == ''):
                        print('Campos vacios')
                        break
                    opciones = webdriver.ChromeOptions()
                    opciones.add_experimental_option(
                        'excludeSwitches', ['enable-automation'])
                    opciones.add_experimental_option(
                        'prefs', {'download.default_directory': 'C:\\TipificacionesDescargadas'})
                    driver = webdriver.Chrome(
                        executable_path=r"driver/chromedriver.exe", chrome_options=opciones)
                    driver.get(
                        "http://crm.colcomercio.com.co/vozcliente/index.php")
                    driver.maximize_window()
                    
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.ID, 'usuario'))).send_keys(usuario)
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '/html/body/div/div/form/div[2]/input'))).send_keys(contraseña)
                    WebDriverWait(driver, 5).until(
                        EC.element_to_be_clickable((By.ID, 'ingresa'))).click()
                    
                    for i in range(5):
                        try:
                            WebDriverWait(driver, 15).until(
                            EC.element_to_be_clickable((By.ID, 'menu-toggle'))).click()
                            WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                            (By.XPATH, '/html/body/div[1]/nav/div/div[3]/ol/li[4]/a'))).click()
                            break
                        except:
                            print('Retry in 3 second')
                            sleep(1)

                    for i in range(5):
                        try:
                            driver.find_element_by_xpath(
                                '/html/body/div[1]/div/div/div[2]/div/form/div[1]/div/div[6]/input').click()
                            break
                        except:
                            print('Retry in 2 second')
                            sleep(1)

                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/div[1]/div[3]/span/div/button'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[13]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[11]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[9]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/div[1]/div[3]/span/div/ul/li[8]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[4]/a/label'))).click()

                    # #Despachos
                    # #******************
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="correo"]'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/button'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[9]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[10]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[11]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[12]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[13]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[23]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[27]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[28]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[29]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[30]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/button'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="generar"]'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="lnk_exporta"]'))).click()
                    sleep(4)
                    driver.back()
                    # driver.quit()

                    # pandas
                    # ***************
                    fname = "C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xls"
                    excel = win32.gencache.EnsureDispatch('Excel.Application')
                    wb = excel.Workbooks.Open(fname)
                    # FileFormat = 51 is for .xlsx extension
                    wb.SaveAs(fname+"x", FileFormat=51)
                    wb.Close()  # FileFormat = 56 is for .xls extension
                    excel.Application.Quit()

                    now = datetime.now()
                    fecha = now.strftime("%m-%d-%Y-%H-%M-%S")

                    df = pd.read_excel(
                        'C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xlsx', header=0)
                    nuevodf = df[df['CREADO_POR'].str.contains(
                        "(Emtelco)", case=False)]
                    nuevodf.to_excel(
                        'TipificacionesModificadas/nuevoReporteDespachosPQR_' + fecha + '.xlsx')

                    remove('C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xls')
                    remove('C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xlsx')

                    print('Tipificacion modificada y guardada, Despachos')

                    # PostVenta
                    # ***************
                    for i in range(5):
                        try:
                            driver.find_element_by_xpath(
                                '/html/body/div[1]/div/div/div[2]/div/form/div[1]/div/div[6]/input').click()
                            break
                        except:
                            print('Retry in 1 second')
                            sleep(1)

                    WebDriverWait(driver, 15).until(EC.element_to_be_clickable(
                        (By.XPATH, '/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/div[1]/div[3]/span/div/button'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[13]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[11]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[9]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '/html/body/div[1]/div/div/div[2]/div/form/div[2]/div/div[1]/div[3]/span/div/ul/li[8]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[1]/div[3]/span/div/ul/li[4]/a/label'))).click()

                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="correo"]'))).click()
                        
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/button'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[14]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[17]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[18]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[21]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="iddatospersonalizados"]/div/div[2]/div[1]/span/div/ul/li[25]/a/label'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="generar"]'))).click()
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="lnk_exporta"]'))).click()
                    sleep(3)
                    driver.quit()
                    # print('Tipificacion modificada y guardada, PostVenta')

                    fname = "C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xls"
                    excel = win32.gencache.EnsureDispatch('Excel.Application')
                    wb = excel.Workbooks.Open(fname)
                    # FileFormat = 51 is for .xlsx extension
                    wb.SaveAs(fname+"x", FileFormat=51)
                    wb.Close()  # FileFormat = 56 is for .xls extension
                    excel.Application.Quit()

                    now = datetime.now()
                    fecha = now.strftime("%m-%d-%Y-%H-%M-%S")

                    df = pd.read_excel(
                        'C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xlsx', header=0)
                    nuevodf = df[df['CREADO_POR'].str.contains(
                        "(Emtelco)", case=False)]
                    nuevodf.to_excel(
                        'TipificacionesModificadas/nuevoReportePostVentaPQR_' + fecha + '.xlsx')

                    remove('C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xls')
                    remove('C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xlsx')

                    print('Tipificacion modificada y guardada, PostVenta')
                    contador += contador
                    sleep(3600)
                except:
                    print('Error')
                    driver.quit()
                    pass
        except:
            pass


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
