import win32com.client as win32
import pandas as pd
from os import remove
from datetime import datetime
fname = "C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xls"
excel = win32.gencache.EnsureDispatch('Excel.Application')
wb = excel.Workbooks.Open(fname)
wb.SaveAs(fname+"x", FileFormat = 51)    #FileFormat = 51 is for .xlsx extension
wb.Close()                               #FileFormat = 56 is for .xls extension
excel.Application.Quit()

# remove('C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xls')
now = datetime.now()
fecha = now.strftime("%m-%d-%Y-%H-%M-%S")

df = pd.read_excel('C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xlsx', header=0)
nuevodf = df[df['CREADO_POR'].str.contains("(Emtelco)", case=False)]
nuevodf.to_excel('TipificacionesModificadas/nuevoReporteDespachosPQR_'+ fecha +'.xlsx')
# nuevodf.to_excel('TipificacionesModificadas/nuevoReporteDespachosPQR.xlsx')

# remove('C:\TipificacionesDescargadas\REPORTE EXCEL PQR.xlsx')
